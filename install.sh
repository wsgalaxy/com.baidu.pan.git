#!/bin/sh

DATATAR="deepin.com.baidu.pan_5.7.3deepin0_i386.deb"
FONTTAR="font.tar.xz"
WINEID="Deepin-BaiduNetDisk"
DESKTOP="com.baidu.pan.desktop"
ICON="com.baidu.pan.svg"


#LIBLDAP="lib32-libldap-2.4.46-1-x86_64.pkg.tar.xz"
#OPENSSL="lib32-openssl-1_1.1.0.h-1-x86_64.pkg.tar.xz"

LIBLDAP="libldap-2.4-2_2.4.44+dfsg-5+deb9u2_i386.deb"
OPENSSL="libssl1.1_1.1.0j-1~deb9u1_i386.deb"
LIBSASL2="libsasl2-2_2.1.27~101-g0780600+dfsg-3_i386.deb"

echo "installing $DATATAR"

ar x $DATATAR
tar xf data.tar.xz
mkdir /app/data
mkdir /app/bin
mkdir /app/ext
cp ./opt/deepinwine/apps/$WINEID/files.7z /app/data/
cp ./$FONTTAR /app/data/
cp ./run.sh /app/bin/
chmod a+x /app/bin/run.sh

mkdir -p /app/share/applications /app/share/icons
cp ./$DESKTOP /app/share/applications
cp ./$ICON /app/share/icons


mkdir /app/lib


echo "installing $LIBLDAP"
mkdir LIBLDAP
cd LIBLDAP
#tar xf ../$LIBLDAP
ar x ../$LIBLDAP
tar xf data.tar.xz
cp ./usr/lib/i386-linux-gnu/*.so* /app/lib
cd ..


echo "installing $OPENSSL"
mkdir OPENSSL
cd OPENSSL
ar x ../$OPENSSL
tar xf data.tar.xz
#tar xf ../$OPENSSL
cp ./usr/lib/i386-linux-gnu/*.so* /app/lib
cd ..

echo "installing $LIBSASL2"
mkdir LIBSASL2
cd LIBSASL2
ar x ../$LIBSASL2
tar xf data.tar.xz
#tar xf ../$OPENSSL
cp ./usr/lib/i386-linux-gnu/*.so* /app/lib
cd ..


echo "done"
